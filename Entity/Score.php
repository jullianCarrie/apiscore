<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use App\Repository\ScoreRepository;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=ScoreRepository::class)
 */
#[ApiResource]
class Score
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;

    public function __construct()
    {
        $this->setDate(new \DateTimeImmutable("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): self // méthode pour avoir la date et l'heure actuelle de façon immuable, le set permet de mettre à jour
    {
        $this->date = $date; //Le $this appelle l'entité présente dans ce code lui attribu la propriété date lié à la variable date

        return $this;
    }
}




