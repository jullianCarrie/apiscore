<?php

namespace App\Controller;

use App\Entity\Score;
use App\Repository\ScoreRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;

const token = "Abcfimq0";

class ScroreController extends AbstractController
{
    #[Route('/scrore', name: 'scrore')]
    public function index(): Response
    {
        return $this->render('scrore/index.html.twig', [
            'controller_name' => 'ScroreController',
        ]);
    }
    #[Route('/serialize', name: 'serialize')]

    public function serializeTest(ScoreRepository $scoreRepo): Response
    {
        if (isset($_GET['token'])) {
            if ($_GET['token'] == token) {
                return $this->json($scoreRepo->findOrderScore(), 200, []);
            } else {
                return $this->json("mauvaise clé de sécu", 401);
            }
        } else {
            return $this->json("clé male définie", 400);
        }

        // $encoders = [new JsonEncoder()];

        // $normalizers = [new ObjectNormalizer()];

        // $pseudo = new Score();
        // $pseudo->setPseudo('');
        // $pseudo->setScore(12);

        // $serializer = new Serializer($normalizers, $encoders);
        // $jsonContent = $serializer->serialize($pseudo, 'json');


        // // On instancie la réponse
        // $response = new Response($jsonContent);

        // // On ajoute l'entête HTTP
        // $response->headers->set('Content-Type', 'application/json');

        // // On envoie la réponse
        // return $response;
        //retourne tous les scores
    }

    #[Route('/scoreuser/{id}', name: 'scoreuser', methods: 'GET')]
    public function lireScore(ScoreRepository $scoreRepo, int $id) //va chercher le repositpry car ne fonctionne pas avec entity
    {
        if (isset($_GET['token'])) {
            if ($_GET['token'] == token) {
                return $this->json($scoreRepo->findBy(array('id' => $id)), 200, []);
            } else {
                return $this->json("mauvaise clé de sécu", 401);
            }
        } else {
            return $this->json("clé male définie", 400);
        }
        //      $score = $scoreRepo->find($id); // fait correspondre le repo avec $score, et lui demande d'aller chercher l'id = technique à faire lorsque pb de 
        //      $encoders = [new JsonEncoder()];
        //      $normalizers = [new ObjectNormalizer()];
        //      $serializer = new Serializer($normalizers, $encoders);
        //      $jsonContent = $serializer->serialize($score, 'json');
        //      $response = new Response($jsonContent);
        //      $response->headers->set('Content-Type', 'application/json');
        //    return $response;

    }

    #[Route('/ajout', name: 'ajout', methods: 'POST')]

    public function addScore(Request $request, SerializerInterface $serializer)
    {
        if (isset($_GET['token'])) {
            if ($_GET['token'] == token) {
                // On vérifie si la requête est une requête Ajax
                //if($request->isXmlHttpRequest()) {
                // On instancie un nouvel article
                //$score = new Score();

                // On décode les données envoyées
                $donnees = $request->getContent();
                $score = $serializer->deserialize($donnees, Score::class, 'json');

                // On met à jour l'objet
                //$score->setScore($donnees->contenu);
                //$score->setPseudo($donnees->titre);
                //$score->setDate($donnees->date);
                //$id = $this->getDoctrine()->getRepository(Users::class)->findOneBy(["id" => 1]);
                //$score->getId($id);

                // On sauvegarde en base de donnée
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($score);
                $entityManager->flush();

                // On retourne la confirmation
                return new Response('ok', 201);

                return new Response('Failed', 404);
            } else {
                return $this->json("mauvaise clé de sécu", 401);
            }
        } else {
            return $this->json("clé male définie", 400);
        }
    }
} 
